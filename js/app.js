var app = angular.module("routing", ['ngRoute','ngStorage']);

// Routing
app.config(function($routeProvider,$locationProvider){

    $routeProvider
        .when("/", {
            templateUrl: "views/home.html",
            controller: "HomeController"
        })
        .when("/about",{
            templateUrl: "views/about.html",
            controller: "AboutController"
        })
        .when("/products",{
            templateUrl: "views/products.html",
            controller: "ProductsController"
        })
        .when("/product/:productId",{
            templateUrl: "views/product.html",
            controller: "ProductController"
        })
        .when("/contact",{
            templateUrl: "views/contact.html",
            controller: "ContactController"
        })
        .otherwise({
            templateUrl: "views/404.html"
        });

    $locationProvider.html5Mode(true);
});