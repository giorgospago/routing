app.controller("ProductsController", function($scope,$http,$localStorage){
    
    $scope.storage = $localStorage.$default({
        min: 0,
        max: 500,
        paok: "ole"
    });

    console.log($scope.storage.min,$scope.storage.max);


    $scope.shoes = [];

    $scope.getShoes = function(){

        $http.get("/ajax/products.json")
            .then(function(response){
                $scope.shoes = response.data;
            });

    };

    $scope.getShoes();
    $scope.myfilter = function(item){
        console.log(item);
        return item.price >= $scope.storage.min && item.price <= $scope.storage.max;
    };

    $scope.reset = function(){
        $scope.storage.min = 0;
        $scope.storage.max = 500;
    };

});