

var a = [1,2,3,4,5,6];

a.filter(function(item){
    return item > 3;
});

console.log(a); // [4,5,6]

// ---------------------------------------------------

var b = ["Giwrgos","Stelios"];

b.map(function(item){
    return item.toUpperCase();
});

console.log(b); // ["GIWRGOS","STELIOS"]

// ---------------------------------------------------

var c = ["nike","adidas","puma"];

c.find(function(item){
    return item == "puma"
})

console.log(c); // "puma"

